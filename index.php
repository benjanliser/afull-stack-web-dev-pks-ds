<?php
// Parent class
abstract class Hewan {
  public $name;
  public $darah = 50;
  public $jumlah_kaki;
  public $keahlian;
  public function __construct($name) {
    $this->name = $name;
  }
  abstract public function atraksi();
}


abstract class Fight {
    public $attackPower;
    public $defencePower;
    
    public function __construct($name) {
      $this->name = $name;
    }
    abstract public function serang();
    abstract public function diserang();
  }


// Child classes
class Harimau extends Hewan {
  public function getInfoHewan(){
    echo "ini adalah hewan  $this->name!";
  }
}

class Elang  extends Hewan {
  public function getInfoHewan() : string{
    echo "ini adalah hewan $this->name!";
  }
}




// Create objects from the child classes
$harimau = new Hewan("harimau1","50","4","berlari cepat");
    echo $harimau->getInfoHewan();
    echo "<br>";

$harimau1 = new Fight();
    echo $harimau1->serang($name->name);
    echo "<br>";
$harimau1 = new Fight();
     echo $harimau1->diserang($name->name);
    echo "<br>";
}

$elang = new Hewan("elang1","50","2","terbang tinggi");
    echo $elang->getInfoHewan();
    echo "<br>";

$elang = new Fight();
    echo $elang->serang($name->name);
    echo "<br>";
$elang = new Fight();
     echo $elang->diserang($name->name);
    echo "<br>";
}




?>
